﻿using LendFoundry.Foundation.Services;
using LendFoundry.Merchant.OfferEngine;
using Microsoft.AspNetCore.Mvc;

namespace LendFoundry.Loans.Filters.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IOfferEngineService service)
        {
            Service = service;
        }

        private IOfferEngineService Service { get; }

        /// <summary>
        /// Creates the offer.
        /// </summary>
        /// <param name="offerType">Type of the offer.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns></returns>
        [HttpPost("{offerType}/{applicationNumber}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IOfferDefinition))]
        public IActionResult CreateOffer(string offerType, string applicationNumber)
        {
            return Execute(() => new OkObjectResult(Service.Create(offerType, applicationNumber)));
        }

        /// <summary>
        /// Selects the offer.
        /// </summary>
        /// <param name="offerType">Type of the offer.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="offerId">The offer identifier.</param>
        /// <returns></returns>
        [HttpPost("{offerType}/{applicationNumber}/{offerId}/select")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public IActionResult SelectOffer(string offerType, string applicationNumber, string offerId)
        {
            return Execute(() => { Service.Select(offerType, applicationNumber, offerId); return new StatusCodeResult(204); });
        }

        /// <summary>
        /// Changes the offer.
        /// </summary>
        /// <param name="offerType">Type of the offer.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        [HttpPost("{offerType}/{applicationNumber}/change/{amount}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IOfferDefinition))]
        public IActionResult ChangeOffer(string offerType, string applicationNumber, string amount)
        {
            return Execute(() => new OkObjectResult(Service.Change(offerType, applicationNumber, amount)));
        }

        /// <summary>
        /// Gets the selected offer.
        /// </summary>
        /// <param name="offerType">Type of the offer.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns></returns>
        [HttpGet("{offerType}/{applicationNumber}/selected")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IOffer))]
        public IActionResult GetSelectedOffer(string offerType, string applicationNumber)
        {
            return Execute(() => new OkObjectResult(Service.GetSelected(offerType, applicationNumber)));
        }

        /// <summary>
        /// Gets the by application.
        /// </summary>
        /// <param name="offerType">Type of the offer.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns></returns>
        [HttpGet("{applicationNumber}")]
        [Consumes("application/json")]
        [Produces("application/json", Type = typeof(IOfferDefinition))]
        public IActionResult GetByApplication(string offerType, string applicationNumber)
        {
            return Execute(() => new OkObjectResult(Service.GetByApplication(applicationNumber)));
        }
    }
}
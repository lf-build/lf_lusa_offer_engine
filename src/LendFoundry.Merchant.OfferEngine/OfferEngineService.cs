﻿using LendFoundry.Merchant.Application;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Linq;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Merchant.OfferEngine
{
    public class OfferEngineService : IOfferEngineService
    {
        public OfferEngineService
        (
            IOfferEngineRepository repository,
            ITenantTime tenantTime,
            IEventHubClient eventHub,
            ILogger logger,
            IConfigurationService<Configuration> configuration,
            IDecisionEngineService decisionEngineClient,
            IApplicationService applicationService
        )
        {
            if (repository == null)
                throw new ArgumentException($"{nameof(repository)} is mandatory");

            if (tenantTime == null)
                throw new ArgumentException($"{nameof(tenantTime)} is mandatory");

            if (eventHub == null)
                throw new ArgumentException($"{nameof(eventHub)} is mandatory");

            if (logger == null)
                throw new ArgumentException($"{nameof(logger)} is mandatory");

            if (configuration == null)
                throw new ArgumentException($"{nameof(configuration)} is mandatory");

            if (decisionEngineClient == null)
                throw new ArgumentException($"{nameof(decisionEngineClient)} is mandatory");

            if (applicationService == null)
                throw new ArgumentException($"{nameof(applicationService)} is mandatory");

            Repository = repository;
            TenantTime = tenantTime;
            EventHub = eventHub;
            Logger = logger;
            Configuration = configuration;
            DecisionEngineClient = decisionEngineClient;
            ApplicationService = applicationService;
        }

        private IApplicationService ApplicationService { get; }

        private IDecisionEngineService DecisionEngineClient { get; }

        private IConfigurationService<Configuration> Configuration { get; }

        private IOfferEngineRepository Repository { get; }

        private ITenantTime TenantTime { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        public IOfferDefinition Create(string offerType, string applicationNumber)
        {
            if (string.IsNullOrEmpty(offerType))
                throw new ArgumentException("OfferType is mandatory");

            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentException("ApplicationNumber is mandatory");

            CheckApplication(applicationNumber);

            IOfferDefinition definition;
            try
            {
                var offerRule = GetOfferConfiguration(offerType);
                definition = Repository.GetBy(offerType, applicationNumber);
                if (definition != null)
                {
                    Logger.Info($"Offer already exists for application #{applicationNumber} and type #{offerType}");
                    return definition;
                }

                var result = DecisionEngineClient.Execute<dynamic, OfferResponse>(offerRule.Name, new
                {
                    payload = new { applicationNumber }
                });

                if (!result.Success)
                    throw new ArgumentException(result.Message);

                definition = result.Offer;
                definition.ApplicationNumber = applicationNumber;
                definition.OfferConfiguration = (OfferConfiguration)offerRule;
                definition.Date = new TimeBucket(TenantTime.Today);
                SetOfferId(definition);
                Repository.Add(definition);
                Logger.Info($"New offer created for application #{applicationNumber}");
            }
            catch (ClientException ex)
            {
                var msg = $"Error while executing rules for #{applicationNumber}, please check decision-engine logs";
                Logger.Error(msg, ex);                
                throw new InvalidOperationException(msg);
            }
            catch (Exception ex) { Logger.Error($"Error while creating new offer for applicantion #{applicationNumber}", ex); throw; }
            EventHub.Publish(new Events.OfferGenerated(definition));
            return definition;
        }

        public void Select(string offerType, string applicationNumber, string offerId)
        {
            if (string.IsNullOrEmpty(offerType))
                throw new ArgumentException("OfferType is mandatory");

            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentException("ApplicationNumber is mandatory");

            if (string.IsNullOrEmpty(offerId))
                throw new ArgumentException("OfferId is mandatory");

            var selectedOfferId = default(int);

            if (!int.TryParse(offerId, out selectedOfferId))
                throw new ArgumentException("OfferId informed is not valid");

            CheckApplication(applicationNumber);

            var application = CheckApplication(applicationNumber);
            if (application.Status != null)
                AllowedChangeOffer(application.Status.Code);

            if (GetOfferConfiguration(offerType) != null)
            {
                var definition = Repository.GetBy(offerType, applicationNumber);

                if (definition == null)
                    throw new NotFoundException($"Cannot find application {applicationNumber} and offer type {offerType}");

                definition.Offers = definition.Offers.Select(o =>
                {
                    o.Selected = Convert.ToInt32(o.Id) == selectedOfferId;
                    return o;
                });

                Repository.Update(definition);
                var selected = definition.Offers.First(o => o.Selected);

                EventHub.Publish(new Events.OfferSelected(definition));
                Logger.Info($"Offer id #{selected.Id} marked as selected for application #{applicationNumber} and type #{offerType}");
                
            }
        }

        public IOfferDefinition Change(string offerType, string applicationNumber, string changingAmount)
        {
            if (string.IsNullOrEmpty(offerType))
                throw new ArgumentException("OfferType is mandatory");
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentException("ApplicationNumber is mandatory");

            var application = CheckApplication(applicationNumber);
            if(application.Status!=null)
            AllowedChangeOffer(application.Status.Code);
                
            double amount;
            if (!double.TryParse(changingAmount, out amount))
                throw new ArgumentException($"Invalid new offer amount {changingAmount}");

            if (amount <= 0)
                throw new ArgumentException("Amount is mandatory");

            var definition = Repository.GetBy(offerType, applicationNumber);

            if (definition == null)
                throw new NotFoundException($"Cannot find application {applicationNumber} and offer type {offerType}");

            if (amount > definition.MaxAmount)
                throw new InvalidOperationException($"New offer amount must to be less than actual max amount {definition.MaxAmount}");

            var offerRule = GetOfferConfiguration(offerType);

            try
            {
                var result = DecisionEngineClient.Execute<dynamic, OfferResponse>(offerRule.Name, new
                {
                    payload = new { applicationNumber, amount }
                });

                if (!result.Success)
                    throw new ArgumentException(result.Message);

                var newOffer = result.Offer;

                definition.ApplicationNumber = applicationNumber;
                definition.MaxAmount = newOffer.MaxAmount;
                definition.Preferred = newOffer.Preferred;
                definition.Offers = newOffer.Offers;
                definition.OfferConfiguration = (OfferConfiguration)offerRule;
                definition.Date = new TimeBucket(TenantTime.Today);

                SetOfferId(definition);

                Repository.Update(definition);

                EventHub.Publish(new Events.OfferChanged(definition));

                Logger.Info($"Offer change applied for application #{applicationNumber}");
            }
            catch (ClientException ex)
            {
                var msg = $"Error while executing rules for #{applicationNumber}, plase check decision-engine logs";
                Logger.Error(msg, ex);
                throw new InvalidOperationException(msg);
            }
            catch (Exception ex) { Logger.Error($"Error while changing offer to applicantion #{applicationNumber}", ex); throw; }
            return definition;
        }

        public IOffer GetSelected(string offerType, string applicationNumber)
        {
            if (string.IsNullOrEmpty(offerType))
                throw new ArgumentException("OfferType is mandatory");

            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentException("ApplicationNumber is mandatory");

            CheckApplication(applicationNumber);
            var definition = Repository.GetBy(offerType, applicationNumber);

            if (definition == null)
                throw new NotFoundException($"Cannot find application {applicationNumber} and offer type {offerType}");

            var selectedOffer = definition.Offers.FirstOrDefault(o => o.Selected);

            if (selectedOffer == null)
                throw new NotFoundException($"No selected offers found");

            return selectedOffer;
        }

        public IOfferDefinition GetByApplication(string applicationNumber)
        {
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentException("ApplicationNumber is mandatory");

            CheckApplication(applicationNumber);
            var definition = Repository.GetBy(applicationNumber);

            if (definition == null)
                throw new NotFoundException($"Cannot find application {applicationNumber}");

            return definition;
        }

        private IApplicationResponse CheckApplication(string applicationNumber)
        {
            var application = ApplicationService.GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} cannot be found");

            return application;
        }

        private void AllowedChangeOffer(string applicationStatus)
        {
            var offerEngineConfiguration = Configuration.Get();
            if (offerEngineConfiguration == null)
                throw new InvalidArgumentException($"The {nameof(offerEngineConfiguration)} cannot be found", nameof(offerEngineConfiguration));

            var allowedStatuses = offerEngineConfiguration.AllowedStatusesForOfferChange;

            if (allowedStatuses == null || !allowedStatuses.Any())
                throw new InvalidArgumentException($"The {nameof(offerEngineConfiguration.AllowedStatusesForOfferChange)} cannot be null", nameof(offerEngineConfiguration.AllowedStatusesForOfferChange));


            if( !allowedStatuses.Contains(applicationStatus) && allowedStatuses.Any())
                throw new InvalidOperationException("Offer can not be changed.");
        }

        private void SetOfferId(IOfferDefinition definition)
        {
            definition.Offers = definition.Offers.Select((o, i) =>
            {
                o.Id = i++.ToString();
                return o;
            });
        }

        private IOfferConfiguration GetOfferConfiguration(string offerType)
        {
            var types = Configuration.Get()?.OfferTypes;

            if (types == null)
            {
                var msg = $"Api configuration cannot be found, please check";
                Logger.Error(msg);
                throw new NotFoundException(msg);
            }

            if (!types.ContainsKey(offerType))
            {
                var msg = $"Offer #{offerType} cannot be found, please check configuration";
                Logger.Error(msg);
                throw new NotFoundException(msg);
            }

            var config = types[offerType];
            config.Type = offerType;

            return config;
        }      
    }
}

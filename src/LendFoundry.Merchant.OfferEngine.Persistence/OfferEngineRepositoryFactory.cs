using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LendFoundry.Merchant.OfferEngine.Persistence
{
    public class OfferEngineRepositoryFactory : IOfferEngineRepositoryFactory
    {
        public OfferEngineRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IOfferEngineRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new OfferEngineRepository(tenantService, mongoConfiguration);
        }
    }
}
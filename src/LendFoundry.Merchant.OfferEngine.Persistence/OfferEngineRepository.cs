﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Linq;

namespace LendFoundry.Merchant.OfferEngine.Persistence
{
    public class OfferEngineRepository : MongoRepository<IOfferDefinition, OfferDefinition>, IOfferEngineRepository
    {
        static OfferEngineRepository()
        {
            BsonClassMap.RegisterClassMap<OfferDefinition>(map =>
            {
                map.AutoMap();
                var type = typeof(OfferDefinition);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Offer>(map =>
            {
                map.AutoMap();                
                var type = typeof(Offer);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<OfferConfiguration>(map =>
            {
                map.AutoMap();
                var type = typeof(OfferConfiguration);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Fee>(map =>
            {
                map.AutoMap();
                map.MapMember(f => f.Type).SetSerializer(new EnumSerializer<FeeType>(BsonType.String));
                var type = typeof(Fee);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Rate>(map =>
            {
                map.AutoMap();
                map.MapMember(f => f.Type).SetSerializer(new EnumSerializer<RateType>(BsonType.String));
                var type = typeof(Rate);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public OfferEngineRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "offers")
        {
            CreateIndexIfNotExists("application-number", Builders<IOfferDefinition>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ApplicationNumber));
        }

        public void Create(IOfferDefinition offerDefinition)
        {
            // add identification for each internal offer
            offerDefinition.Offers = offerDefinition.Offers.Select((o, i) =>
            {
                o.Id = i++.ToString();
                return o;
            });
            Add(offerDefinition);
        }

        public IOfferDefinition GetBy(string offerType, string applicationNumber)
        {
            return Query.FirstOrDefault(f => f.ApplicationNumber == applicationNumber && f.OfferConfiguration.Type == offerType);
        }

        public IOfferDefinition GetBy(string applicationNumber)
        {
            return Query.Where(f => f.ApplicationNumber == applicationNumber).FirstOrDefault();
        }
    }
}

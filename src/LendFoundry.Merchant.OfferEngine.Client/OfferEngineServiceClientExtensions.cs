﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Merchant.OfferEngine.Client
{
    public static class OfferEngineServiceClientExtensions
    {
        public static IServiceCollection AddOfferEngine(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IOfferEngineServiceFactory>(p => new OfferEngineServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IOfferEngineServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}

﻿using System;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace LendFoundry.Merchant.OfferEngine.Client
{
    public class OfferEngineServiceClient : IOfferEngineService
    {
        public OfferEngineServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IOfferDefinition Change(string offerType, string applicationNumber, string amount)
        {
            var request = new RestRequest("/{offerType}/{applicationNumber}/change/{amount}", Method.POST);
            request.AddUrlSegment("offerType", offerType);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("amount", amount);
            return Client.Execute<OfferDefinition>(request);
        }

        public IOfferDefinition Create(string offerType, string applicationNumber)
        {
            var request = new RestRequest("/{offerType}/{applicationNumber}", Method.GET);
            request.AddUrlSegment("offerType", offerType);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            return Client.Execute<OfferDefinition>(request);
        }

        public IOfferDefinition GetByApplication(string applicationNumber)
        {
            var request = new RestRequest("/{applicationNumber}", Method.GET);            
            request.AddUrlSegment("applicationNumber", applicationNumber);
            return Client.Execute<OfferDefinition>(request);
        }

        public IOffer GetSelected(string offerType, string applicationNumber)
        {
            var request = new RestRequest("/{offerType}/{applicationNumber}/selected", Method.GET);
            request.AddUrlSegment("offerType", offerType);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            return Client.Execute<Offer>(request);
        }

        public void Select(string offerType, string applicatinoNumber, string offerId)
        {
            var request = new RestRequest("/{offerType}/{applicationNumber}/{offerId}/select", Method.GET);
            request.AddUrlSegment("offerType", offerType);
            request.AddUrlSegment("applicatinoNumber", applicatinoNumber);
            request.AddUrlSegment("offerId", offerId);
            Client.Execute(request);
        }
    }
}

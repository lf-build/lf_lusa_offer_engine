﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Merchant.OfferEngine.Client
{
    public class OfferEngineServiceClientFactory : IOfferEngineServiceFactory
    {
        public OfferEngineServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; set; }

        private string Endpoint { get; set; }

        private int Port { get; set; }

        public IOfferEngineService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new OfferEngineServiceClient(client);
        }
    }
}

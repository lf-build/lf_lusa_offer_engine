﻿namespace LendFoundry.Merchant.OfferEngine
{
    public enum RateType
    {
        Percent,
        Factor
    }
}

﻿using System.Collections.Generic;

namespace LendFoundry.Merchant.OfferEngine
{
    public class Configuration
    {
        public Dictionary<string, OfferConfiguration> OfferTypes { get; set; }
        public IEnumerable<string> AllowedStatusesForOfferChange { get; set; }
    }
}

﻿namespace LendFoundry.Merchant.OfferEngine
{
    public class Fee : IFee
    {
        public FeeType Type { get; set; }
        public double Amount { get; set; }
    }
}

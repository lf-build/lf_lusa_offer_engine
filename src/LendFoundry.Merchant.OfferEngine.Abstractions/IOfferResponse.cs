﻿namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferResponse
    {
        bool Success { get; set; }
        string Message { get; set; }
        OfferDefinition Offer { get; set; }
    }
}
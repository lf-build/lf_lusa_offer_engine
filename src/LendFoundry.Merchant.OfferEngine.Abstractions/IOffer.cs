﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOffer
    {
        string Id { get; set; }
        int Terms { get; set; }
        bool Selected { get; set; }
        Rate Rate { get; set; }
        double OfferAmount { get; set; }
        double PaymentAmount { get; set; }
        double Apr { get; set; }
        IEnumerable<Fee> Fees { get; set; }
        double PayoutAmount { get; set; }
        double Payout { get; set; }
        DateTimeOffset ExpirationDate { get; set; }                       
    }
}

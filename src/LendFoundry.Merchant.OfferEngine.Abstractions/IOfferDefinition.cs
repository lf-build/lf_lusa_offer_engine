﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferDefinition: IAggregate
    {
        string ApplicationNumber { get; set; }        
        double MaxAmount { get; set; }
        int Preferred { get; set; }
        IEnumerable<Offer> Offers { get; set; }                    
        OfferConfiguration OfferConfiguration { get; set;}
        TimeBucket Date { get; set; }
    }
}
﻿namespace LendFoundry.Merchant.OfferEngine
{
    public class OfferConfiguration : IOfferConfiguration
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
    }
}

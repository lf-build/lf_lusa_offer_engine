﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Merchant.OfferEngine
{
    public class OfferDefinition : Aggregate, IOfferDefinition
    {
        public string ApplicationNumber { get; set; }        
        public double MaxAmount { get; set; }
        public int Preferred { get; set; }
        public IEnumerable<Offer> Offers { get; set; }                
        public OfferConfiguration OfferConfiguration { get; set; }
        public TimeBucket Date { get; set; }
    }
}

﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferEngineRepository : IRepository<IOfferDefinition>
    {
        void Create(IOfferDefinition offerDefinition);        
        IOfferDefinition GetBy(string offerType, string applicationNumber);
        IOfferDefinition GetBy(string applicationNumber);
    }
}

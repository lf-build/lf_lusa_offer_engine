﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferEngineServiceFactory
    {
        IOfferEngineService Create(ITokenReader reader);
    }
}

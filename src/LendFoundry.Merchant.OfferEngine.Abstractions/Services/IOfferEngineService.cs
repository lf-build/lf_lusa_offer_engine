﻿namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferEngineService
    {
        IOfferDefinition Create(string offerType, string applicationNumber);
        void Select(string offerType, string applicatinoNumber, string offerId);
        IOfferDefinition Change(string offerType, string applicationNumber, string amount);
        IOffer GetSelected(string offerType, string applicationNumber);
        IOfferDefinition GetByApplication(string applicationNumber);
    }
}

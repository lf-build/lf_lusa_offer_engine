using LendFoundry.Security.Tokens;

namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferEngineRepositoryFactory
    {
        IOfferEngineRepository Create(ITokenReader reader);
    }
}
﻿namespace LendFoundry.Merchant.OfferEngine.Events
{
    public class OfferChanged
    {
        public OfferChanged(IOfferDefinition offer)
        {
            Offer = offer;
        }

        public IOfferDefinition Offer { get; set; }
    }
}

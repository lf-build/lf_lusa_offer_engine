﻿namespace LendFoundry.Merchant.OfferEngine.Events
{
    public class OfferSelected
    {
        public OfferSelected(IOfferDefinition offer)
        {
            Offer = offer;
        }

        public IOfferDefinition Offer { get; set; }
    }
}

﻿namespace LendFoundry.Merchant.OfferEngine.Events
{
    public class OfferGenerated
    {
        public OfferGenerated(IOfferDefinition offer)
        {
            Offer = offer;
        }

        public IOfferDefinition Offer { get; set; }
    }
}

﻿namespace LendFoundry.Merchant.OfferEngine
{
    public interface IOfferConfiguration
    {
        string Type { get; set; }
        string Name { get; set; }
        string Version { get; set; }
    }
}

﻿namespace LendFoundry.Merchant.OfferEngine
{
    public interface IRate
    {
        RateType Type { get; set; }
        double Amount { get; set; }
    }
}

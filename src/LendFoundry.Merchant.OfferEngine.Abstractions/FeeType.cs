﻿namespace LendFoundry.Merchant.OfferEngine
{
    public enum FeeType
    {
        Fixed,
        Percent
    }
}

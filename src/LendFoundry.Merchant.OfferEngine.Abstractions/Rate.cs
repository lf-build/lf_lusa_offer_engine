﻿namespace LendFoundry.Merchant.OfferEngine
{
    public class Rate : IRate
    {
        public RateType Type { get; set; }
        public double Amount { get; set; }
    }
}

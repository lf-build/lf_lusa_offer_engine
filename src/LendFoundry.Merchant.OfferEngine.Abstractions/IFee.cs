﻿namespace LendFoundry.Merchant.OfferEngine
{
    public interface IFee
    {
        FeeType Type { get; set; }
        double Amount { get; set; }
    }
}

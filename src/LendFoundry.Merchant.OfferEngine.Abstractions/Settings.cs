﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Merchant.OfferEngine
{
    public static class Settings
    { 
        private const string Prefix = "OFFER-ENGINE";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");                

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE", "decision-engine");

        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION", "application");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", Prefix.ToLower());
    }
}

﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Merchant.OfferEngine
{
    public class OfferResponse : Aggregate, IOfferResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public OfferDefinition Offer { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Merchant.OfferEngine
{
    public class Offer : IOffer
    {
        public string Id { get; set; }
        public int Terms { get; set; }
        public bool Selected { get; set; }
        public Rate Rate { get; set; }
        public double OfferAmount { get; set; }
        public double PaymentAmount { get; set; }
        public double Apr { get; set; }
        public IEnumerable<Fee> Fees { get; set; }
        public double PayoutAmount { get; set; } = 0.0;
        public double Payout { get; set; } = 0.0;
        public DateTimeOffset ExpirationDate { get; set; }
        public int RiskGroup { get; set; }
    }
}

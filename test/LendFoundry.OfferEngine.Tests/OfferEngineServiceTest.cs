﻿using LendFoundry.Application;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using LendFoundry.Clients.DecisionEngine;
using Xunit;

namespace LendFoundry.OfferEngine.Tests
{
    public class OfferEngineServiceTest
    {
        [Fact]
        public void InitOfferEngineServiceWhenNoRepository()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  null,
                  Mock.Of<ITenantTime>(),
                  Mock.Of<IEventHubClient>(),
                  Mock.Of<ILogger>(),
                  Mock.Of<IConfigurationService<Configuration>>(),
                  Mock.Of<IDecisionEngineService>(),
                  Mock.Of<IApplicationService>()
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenNoTenantTime()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  Mock.Of<IOfferEngineRepository>(),
                  null,
                  Mock.Of<IEventHubClient>(),
                  Mock.Of<ILogger>(),
                  Mock.Of<IConfigurationService<Configuration>>(),
                  Mock.Of<IDecisionEngineService>(),
                  Mock.Of<IApplicationService>()
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenNoEventhub()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  Mock.Of<IOfferEngineRepository>(),
                  Mock.Of<ITenantTime>(),
                  null,
                  Mock.Of<ILogger>(),
                  Mock.Of<IConfigurationService<Configuration>>(),
                  Mock.Of<IDecisionEngineService>(),
                  Mock.Of<IApplicationService>()
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenNoLogger()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  Mock.Of<IOfferEngineRepository>(),
                  Mock.Of<ITenantTime>(),
                  Mock.Of<IEventHubClient>(),
                  null,
                  Mock.Of<IConfigurationService<Configuration>>(),
                  Mock.Of<IDecisionEngineService>(),
                  Mock.Of<IApplicationService>()
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenNoConfiguration()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  Mock.Of<IOfferEngineRepository>(),
                  Mock.Of<ITenantTime>(),
                  Mock.Of<IEventHubClient>(),
                  Mock.Of<ILogger>(),
                  null,
                  Mock.Of<IDecisionEngineService>(),
                  Mock.Of<IApplicationService>()
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenNoDecisionEngine()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  Mock.Of<IOfferEngineRepository>(),
                  Mock.Of<ITenantTime>(),
                  Mock.Of<IEventHubClient>(),
                  Mock.Of<ILogger>(),
                  Mock.Of<IConfigurationService<Configuration>>(),
                  null,
                  Mock.Of<IApplicationService>()
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenNoApplication()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new OfferEngineService
                (
                  Mock.Of<IOfferEngineRepository>(),
                  Mock.Of<ITenantTime>(),
                  Mock.Of<IEventHubClient>(),
                  Mock.Of<ILogger>(),
                  Mock.Of<IConfigurationService<Configuration>>(),
                  Mock.Of<IDecisionEngineService>(),
                  null
                );
            });
        }

        [Fact]
        public void InitOfferEngineServiceWhenOperationOk()
        {
            var service = new OfferEngineService
            (
                Mock.Of<IOfferEngineRepository>(),
                Mock.Of<ITenantTime>(),
                Mock.Of<IEventHubClient>(),
                Mock.Of<ILogger>(),
                Mock.Of<IConfigurationService<Configuration>>(),
                Mock.Of<IDecisionEngineService>(),
                Mock.Of<IApplicationService>()
            );
            Assert.IsType<OfferEngineService>(service);
        }

        [Fact]
        public void CreateWhenOfferTypeNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Create("", "app001");
            });
        }

        [Fact]
        public void CreateWhenApplicationNumberNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Create("fha-mortgage", "");
            });
        }

        [Fact]
        public void CreateWhenConfigurationNotInformed()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService(null, null, new Configuration(), null);
                service.Create("fha-mortgage", "app001");
            });
        }

        [Fact]
        public void CreateWhenOfferTypeNotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.Create("fha-mortgage-X", "app001");
            });
        }

        [Fact]
        public void CreateWhenExecutionOk()
        {
            var repo = new FakeRepository();
            var service = GetService(repo);
            var result = service.Create("fha-mortgage", "app001");

            Assert.NotNull(result);
            Assert.NotEmpty(result.Id);

            var dbResult = repo.GetBy("fha-mortgage", "app001");
            Assert.NotNull(dbResult);
            Assert.Equal(result.Id, dbResult.Id);
        }

        [Fact]
        public void CreateWhenOfferAlreadyExists()
        {
            var repo = new FakeRepository();
            var key = Guid.NewGuid().ToString("N");
            var definition = new OfferDefinition
            {
                Id = key,
                ApplicationNumber = "app001",
                OfferConfiguration = new OfferConfiguration
                {
                    Name = "fha-mortgage"
                },
                Offers = new[]
                {
                    new Offer { }
                }
            };
            repo.Create(definition);
            var service = GetService(repo);
            var result = service.Create("fha-mortgage", "app001");

            Assert.NotNull(result);
            Assert.NotEmpty(result.Id);
            Assert.Equal(result.Id, key);
        }

        [Fact]
        public void CreateWhenDecisionEngineRisesException()
        {
            var _applicationNumber = "app0001";
            var decisionEngine = new Mock<IDecisionEngineService>();
            decisionEngine.Setup(s => s.Execute<dynamic, OfferDefinition>(It.IsAny<string>(), It.IsAny<object>())).Throws<Exception>();
            var service = GetService(null, null, null, decisionEngine.Object);

            Assert.Throws<Exception>(() =>
            {
                service.Create("fha-mortgage", _applicationNumber);
            });
        }

        [Fact]
        public void SelectWhenOfferTypeNotInformed()
        {
            Assert.ThrowsAny<ArgumentException>(() =>
            {
                GetService().Select("", "app001", "1");
            });
        }

        [Fact]
        public void SelectWhenApplicationNumberNotInformed()
        {
            Assert.ThrowsAny<ArgumentException>(() =>
            {
                GetService().Select("fha-mortgage", "", "1");
            });
        }

        [Fact]
        public void SelectWhenOfferIdNotInformed()
        {
            Assert.ThrowsAny<ArgumentException>(() =>
            {
                GetService().Select("fha-mortgage", "app001", "");
            });
        }

        [Fact]
        public void SelectWhenInformedInvalidOfferId()
        {
            Assert.ThrowsAny<ArgumentException>(() =>
            {
                GetService().Select("fha-mortgage", "app001", "x");
            });
        }

        [Fact]
        public void SelectWhenNoConfigurationFound()
        {
            Assert.ThrowsAny<NotFoundException>(() =>
            {
                GetService(null, null, new Configuration()).Select("fha-mortgage", "app001", "1");
            });
        }

        [Fact(Skip ="removed offer expiration")]
        public void SelectWhenOfferAlreadyExpired()
        {
            Assert.ThrowsAny<InvalidOperationException>(() =>
            {
                var repo = new FakeRepository();
                var key = Guid.NewGuid().ToString("N");
                var definition = new OfferDefinition
                {
                    Id = key,
                    ApplicationNumber = "app001",
                    OfferConfiguration = new OfferConfiguration
                    {
                        Name = "fha-mortgage",
                        //ExpirationDays = 1
                    },
                    Offers = new[] { new Offer { Id = "1" } },
                    Date = new TimeBucket(new DateTime(2016, 06, 01))
                };
                repo.Create(definition);
                GetService(repo).Select("fha-mortgage", "app001", "1");
            });
        }

        [Fact]
        public void SelectWhenApplicationDoesNotExists()
        {
            Assert.ThrowsAny<NotFoundException>(() =>
            {
                var repo = new FakeRepository();
                var key = Guid.NewGuid().ToString("N");
                var definition = new OfferDefinition
                {
                    Id = key,
                    ApplicationNumber = "app001",
                    OfferConfiguration = new OfferConfiguration
                    {
                        Name = "fha-mortgage",
                        //ExpirationDays = 1
                    },
                    Offers = new[] { new Offer { Id = "1" } },
                    Date = new TimeBucket(new DateTime(2016, 06, 01))
                };
                repo.Create(definition);
                var service = GetService(repo);
                service.Select("fha-mortgage", "app002", "1");
            });
        }

        [Fact]
        public void SelectWhenExecutionOk()
        {
            var repo = new FakeRepository();
            var key = Guid.NewGuid().ToString("N");
            var definition = new OfferDefinition
            {
                Id = key,
                ApplicationNumber = "app001",
                OfferConfiguration = new OfferConfiguration
                {
                    Name = "fha-mortgage",
                    //ExpirationDays = 1
                },
                Offers = new[] {
                    new Offer { OfferAmount = 2000 },
                    new Offer { OfferAmount = 3000 },
                    new Offer { OfferAmount = 4000 },
                },
                Date = new TimeBucket(new DateTime(2016, 06, 01))
            };
            repo.Create(definition);
            var service = GetService(repo);
            service.Select("fha-mortgage", "app001", "1");
            var result = service.GetSelected("fha-mortgage", "app001");

            Assert.Equal(3000, result.OfferAmount);
            Assert.Equal("1", result.Id);
        }

        [Fact]
        public void ChangeWhenOfferTypeNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Change("", "app001", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenApplicationNumberNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Change("default", "", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenAmountNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Change("default", "app001", "");
            });
        }

        [Fact]
        public void ChangeWhenAmountIsInvalid()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.Change("default", "app001", "xxxx");
            });
        }

        [Fact]
        public void ChangeWhenOfferNotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.Change("default", "app001", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenAmountGreaterThenMaxAmount()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var repo = new FakeRepository();
                var key = Guid.NewGuid().ToString("N");
                var definition = new OfferDefinition
                {
                    Id = key,
                    ApplicationNumber = "app001",
                    OfferConfiguration = new OfferConfiguration
                    {
                        Name = "fha-mortgage",
                        //ExpirationDays = 1
                    },
                    Offers = new[] {
                        new Offer { OfferAmount = 2000 },
                        new Offer { OfferAmount = 3000 },
                        new Offer { OfferAmount = 4000 },
                    },
                    Date = new TimeBucket(new DateTime(2016, 06, 01)),
                    MaxAmount = 20000.00
                };
                repo.Create(definition);
                var service = GetService(repo);
                service.Change("fha-mortgage", "app001", "25000.00");
            });
        }

        [Fact]
        public void ChangeWhenConfigurationNotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService(null, null, new Configuration());
                service.Change("default", "app001", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenOfferTypeNotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService(null, null, new Configuration());
                service.Change("invalid-offer-type", "app001", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenAllowedChangeOfferConfigurationNotFound()
        {
            var mock = new Mock<IApplicationService>();
            IApplicationResponse applicationResponse = Mock.Of<IApplicationResponse>();
            applicationResponse.Status = new Status { Code = "500.01" };
            applicationResponse.ApplicationNumber = "app0001";
            mock.Setup(s => s.GetByApplicationNumber(It.IsAny<string>())).Returns(applicationResponse);


            Assert.Throws<InvalidArgumentException>(() =>
            {
                var service = GetService(null, null, new Configuration() { AllowedStatusesForOfferChange = null }, null, mock.Object);
                service.Change("default", "app001", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenAllowedStatusesForOfferChangeNotFound()
        {
            var mock = new Mock<IApplicationService>();
            IApplicationResponse applicationResponse = Mock.Of<IApplicationResponse>();
            applicationResponse.Status = new Status { Code = "500.01" };
            applicationResponse.ApplicationNumber = "app0001";
            mock.Setup(s => s.GetByApplicationNumber(It.IsAny<string>())).Returns(applicationResponse);

            var repo = new FakeRepository();
            var key = Guid.NewGuid().ToString("N");
            var definition = new OfferDefinition
            {
                Id = key,
                ApplicationNumber = "app001",
                OfferConfiguration = new OfferConfiguration
                {
                    Name = "fha-mortgage",
                    //ExpirationDays = 1
                },
                Offers = new[] {
                        new Offer { OfferAmount = 2000 },
                        new Offer { OfferAmount = 3000 },
                        new Offer { OfferAmount = 4000 },
                    },
                Date = new TimeBucket(new DateTime(2016, 06, 01)),
                MaxAmount = 20000.00
            };
            repo.Create(definition);
            var decisionEngine = new FakeDecisionEngine();
            var service = GetService(repo, null, null, decisionEngine, mock.Object);

            Assert.Throws<InvalidOperationException>(() =>
            {

                service.Change("fha-mortgage", "app001", "15000.00");
            });
        }

        [Fact]
        public void ChangeWhenExecutionOk()
        {
            var repo = new FakeRepository();
            var key = Guid.NewGuid().ToString("N");
            var definition = new OfferDefinition
            {
                Id = key,
                ApplicationNumber = "app001",
                OfferConfiguration = new OfferConfiguration
                {
                    Name = "fha-mortgage",
                    //ExpirationDays = 1
                },
                Offers = new[] {
                        new Offer { OfferAmount = 2000 },
                        new Offer { OfferAmount = 3000 },
                        new Offer { OfferAmount = 4000 },
                    },
                Date = new TimeBucket(new DateTime(2016, 06, 01)),
                MaxAmount = 20000.00
            };
            repo.Create(definition);
            var decisionEngine = new FakeDecisionEngine();
            var service = GetService(repo, null, null, decisionEngine);
            var newOffer = service.Change("fha-mortgage", "app001", "15000.00");

            Assert.Equal(newOffer.ApplicationNumber, "app001");
            Assert.Equal(newOffer.Id, key);
        }

        [Fact]
        public void GetSelectedWhenOfferTypeNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetSelected("", "app001");
            });
        }

        [Fact]
        public void GetSelectedWhenApplicationNumberNotInformed()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetSelected("fha-mortgage", "");
            });
        }

        [Fact]
        public void GetSelectedWhenOfferNotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.GetSelected("non-exists-rule", "app001");
            });
        }

        [Fact]
        public void GetSelectedWhenNoSelectedOfferFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.GetSelected("fha-mortgage", "app001");
            });
        }

        [Fact]
        public void GetSelectedWhenExecutionOk()
        {
            var repo = new FakeRepository();
            var definition = GetData();
            definition.Id = Guid.NewGuid().ToString("N");
            repo.Create(definition);
            var service = GetService(repo);
            var result = service.GetSelected("fha-mortgage", "app001");

            Assert.NotNull(result);
            Assert.Equal(result.Selected, true);
            Assert.Equal(result.OfferAmount, 2000);
            Assert.Equal(result.Apr, 12.5);
            Assert.Equal(result.PaymentAmount, 89.90);
        }

        [Fact]
        public void GetByApplicationWhenApplicationNumberIsInvalid()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByApplication("");
            });

            Assert.Throws<ArgumentException>(() =>
            {
                var service = GetService();
                service.GetByApplication(string.Empty);
            });
        }

        [Fact]
        public void GetByApplicationWhenApplicationNotFound()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                var service = GetService();
                service.GetByApplication("app0001");
            });
        }

        [Fact]
        public void GetByApplicationWhenItHasData()
        {
            var _appNumber = "app0001";
            var repository = new FakeRepository();
            var item1 = GetData();
            item1.ApplicationNumber = _appNumber;
            item1.MaxAmount = 20000;
            item1.Preferred = 0;
            repository.Add(item1);
            var service = GetService(repository);

            var result = service.GetByApplication(_appNumber);
            Assert.NotNull(result);
            Assert.Equal(20000, result.MaxAmount);
            Assert.Equal(0, result.Preferred);
        }

        private IOfferEngineService GetService
        (
           IOfferEngineRepository repository = null,
           IEventHubClient eventhub = null,
           Configuration configuration = null,
           IDecisionEngineService decisionEngine = null,
           IApplicationService applicationService = null
        )
        {
            IOfferEngineRepository inTimeRepository = repository;
            if (inTimeRepository == null)
                inTimeRepository = new FakeRepository();

            IEventHubClient inTimeEventhub = eventhub;
            if (inTimeEventhub == null)
                inTimeEventhub = new FakeEventHub();

            Configuration inTimeConfiguration = configuration;
            if (inTimeConfiguration == null)
                inTimeConfiguration = new Configuration
                {
                    OfferTypes = new Dictionary<string, OfferConfiguration>
                    {
                        {
                            "fha-mortgage", new OfferConfiguration
                            {
                                //ExpirationDays = 1,
                                Name = "fha-mortgage",
                                Version = "1.0"
                            }
                        },
                        {
                            "veterans-affairs", new OfferConfiguration
                            {
                                //ExpirationDays = 2,
                                Name = "veterans-affairs",
                                Version = "1.0"
                            }
                        }
                    },
                    AllowedStatusesForOfferChange = new[] { "200.01" }
                };
            var mockConfiguration = new Mock<IConfigurationService<Configuration>>();
            mockConfiguration.Setup(s => s.Get()).Returns(inTimeConfiguration);

            IDecisionEngineService inTimeDecisionEngine = decisionEngine;
            if (inTimeDecisionEngine == null)
                inTimeDecisionEngine = new FakeDecisionEngine();

            IApplicationService inTimeApplicationService = applicationService;
            if (inTimeApplicationService == null)
            {
                var mock = new Mock<IApplicationService>();
                IApplicationResponse applicationResponse = Mock.Of<IApplicationResponse>();
                //  applicationResponse.Status = new Status { Code = "500.01" };
                applicationResponse.ApplicationNumber = "app0001";
                mock.Setup(s => s.GetByApplicationNumber(It.IsAny<string>())).Returns(applicationResponse);
                inTimeApplicationService = mock.Object;

            }

            return new OfferEngineService(inTimeRepository, Mock.Of<ITenantTime>(), inTimeEventhub,
                Mock.Of<ILogger>(), mockConfiguration.Object, inTimeDecisionEngine, inTimeApplicationService);
        }

        private IOfferDefinition GetData()
        {
            return new OfferDefinition
            {
                Id = string.Empty,
                ApplicationNumber = "app001",
                OfferConfiguration = new OfferConfiguration
                {
                    Name = "fha-mortgage",
                    //ExpirationDays = 1
                },
                Offers = new[] {
                        new Offer { OfferAmount = 2000 ,RiskGroup = 8, Apr = 12.5, Terms = 12, PaymentAmount = 89.90, Selected = true},
                        new Offer { OfferAmount = 3000 ,RiskGroup = 8, Apr = 10.2, Terms = 24, PaymentAmount = 72.68, Selected = false},
                        new Offer { OfferAmount = 4000 ,RiskGroup = 8, Apr = 8.3, Terms = 36, PaymentAmount = 68.90, Selected = false},
                    },
                Date = new TimeBucket(new DateTime(2016, 06, 01)),
                MaxAmount = 20000.00
            };
        }
    }
}

﻿using LendFoundry.EventHub.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.OfferEngine.Tests
{
    public class FakeEventHub : IEventHubClient
    {
        public void On(string eventName, Action<EventInfo> handler)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Publish<T>(T @event)
        {
            return Task.Run(() => true);
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public Task<bool> Publish<T>(string eventName, T @event)
        {
            return Task.Run(() => true);
        }

        public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void StartAsync()
        {
            throw new NotImplementedException();
        }
    }
}

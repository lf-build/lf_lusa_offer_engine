﻿using System;
using LendFoundry.Clients.DecisionEngine;

namespace LendFoundry.OfferEngine.Tests
{
    public class FakeDecisionEngine : IDecisionEngineService
    {
        public DecisionEngineResult<TResult> Execute<TParameter, TResult>(string name, TParameter parameter)
        {
            var data = new OfferDefinition
            {
                ApplicationNumber = null,
                Date = null,                
                Id = null,
                MaxAmount = 22000.00,
                OfferConfiguration = null,
                Offers = new[] {
                        new Offer {
                            Apr = 6.5,
                            RiskGroup = 8,
                            PaymentAmount = 80.98,
                            OfferAmount = 15000.00,
                            Rate = new Rate { Amount = 13.2, Type = RateType.Percent },
                            Selected = false,
                            Terms = 12,
                            Fees = new[] { new Fee { Amount = 10.00, Type = FeeType.Percent } },
                        }
                    },
                Preferred = 0,
                TenantId = null
            };
            
            return new DecisionEngineResult<TResult>
            {
                Output = (TResult)Convert.ChangeType(data, typeof(TResult))
            };
        }

        public dynamic Execute<TParameter>(string name, TParameter parameter)
        {
            throw new NotImplementedException();
        }

        TDecisionEngineResult IDecisionEngineService.Execute<TParameter, TDecisionEngineResult>(string name, TParameter parameter)
        {
            throw new NotImplementedException();
        }
    }
}

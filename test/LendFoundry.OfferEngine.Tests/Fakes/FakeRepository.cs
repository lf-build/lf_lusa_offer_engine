﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.OfferEngine.Tests
{
    public class FakeRepository : IOfferEngineRepository
    {
        public List<IOfferDefinition> InMemoryData { get; set; } = new List<IOfferDefinition>();

        public void Add(IOfferDefinition item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InMemoryData.Add(item);
        }

        public Task<IEnumerable<IOfferDefinition>> All(Expression<Func<IOfferDefinition, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return InMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public bool Any(string applicationNumber)
        {
            return InMemoryData
             .AsQueryable()
             .Any(x => x.ApplicationNumber == applicationNumber);
        }

        public int Count(Expression<Func<IOfferDefinition, bool>> query)
        {
            return InMemoryData
                           .AsQueryable()
                           .Count(query);
        }

        public void Create(IOfferDefinition offerDefinition)
        {
            offerDefinition.Offers = offerDefinition.Offers.Select((o, i) =>
            {
                o.Id = i++.ToString();
                return o;
            });
            InMemoryData.Add(offerDefinition);
        }

        public Task<IOfferDefinition> Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IOfferDefinition> GetAll()
        {
            return InMemoryData;
        }

        public IOfferDefinition GetBy(string applicationNumber)
        {
            return InMemoryData.FirstOrDefault(x => x.ApplicationNumber == applicationNumber);
        }

        public IOfferDefinition GetBy(string offerType, string applicationNumber)
        {
            return InMemoryData
                .Where(x => x.OfferConfiguration.Name == offerType && x.ApplicationNumber == applicationNumber)
                .FirstOrDefault();
        }

        public void Remove(IOfferDefinition item)
        {
            throw new NotImplementedException();
        }

        public void Update(IOfferDefinition item)
        {
            var index = InMemoryData.IndexOf(item);
            InMemoryData[index] = item;
        }
    }
}
